"""
The project is intended for registration on the Python Development Course (SoftServe).
The project performs the Runge-Kutta fourth order method
Written by Pylyp Volodin
"""
import math
import numpy

def integration(integrand, integ_range, y_0=0, step=0.001):
    """The function performs integration of a given function
    by Runge-Kutta fourth order method.
    integrand - function which is integrated. The function has two parameters: x, y;
    integ_range - integration range, which is represented by a list with
    start value integ_range[0] and end value integ_range[1];
    y_0 - intitial function value y_0=y(0) (for differential equations);
    step - integration step.
    """

    #Runge-Kutta coefficients for each Runge-Kutta itеration
    coef = numpy.array([1., 2., 2., 1.])*step/6.

    #Array of steps for calculation of each Runge-Kutta itеration coefficient
    steps = numpy.array([0., 1./2., 1./2., 1.])*step

    #x_var - integration variable, initially in the beginning of integration range
    x_var = integ_range[0]

    #iteration_value[1:]- Runge-Kutta iteration coefficients, with same amount as coef size
    #iteration_value[0]- an integration result of a previous integration step
    # total size of itaration_value[] equals len(coef)+1
    iteration_value = numpy.array([y_0]+list(coef))

    #Cicle of integration steps within the given integration range
    while x_var <= integ_range[1]-step:
        #Calculation of Runge-Kutta itеration coefficients
        for i in range(1, len(iteration_value)):
            iteration_value[i] = integrand(x_var+steps[i-1],\
                                iteration_value[0] + iteration_value[i-1]*steps[i-1])
        #integration result at the current step
        iteration_value[0] += numpy.dot(iteration_value[1:], coef)
        x_var += step

    return iteration_value[0] #The result of integration

def runge_kutta_test_function():
    """Testing function of Kunge-Kutta method integration """

    print("\n\n========\nTESTING OF RUNGE_KUTTA INTEGRATION FUNCTION")

    the_range = [0., 10.] #intagration range

    print("\n\nTested integration Range:", the_range)

    print("\n\nFor integrand function f(x,y)=0 the integral eqals 0")
    result = integration(lambda x, y: 0, the_range)
    print("Calculation Result=% 5.2f; Real Result=% 5.2f; Error=% 5.2e" % (result, 0, result))

    print("\n\nFor integrand function f(x,y)=1 the integral eqals Range[1]-Renge[0]")
    result = integration(lambda x, y: 1, the_range)
    real_result = the_range[1]-the_range[0]
    print("Calculation Result=% 5.2f; Real Result=% 5.2f; Error=% 5.2e" % \
         (result, real_result, result-real_result))

    print("\n\nFor integrand function f(x,y)=k=100 the integral eqals k*(Range[1]-Renge[0])")
    result = integration(lambda x, y: 100, the_range)
    real_result = 100*(the_range[1]-the_range[0])
    print("Calculation Result=% 5.2f; Real Result=% 5.2f; Error=% 5.2e" % \
         (result, real_result, result-real_result))

    print("\n\nFor integrand function f(x,y)=x the integral eqals (Range[1]^2-Renge[0]^2)/2.")
    result = integration(lambda x, y: x, the_range)
    real_result = (the_range[1]*the_range[1]-the_range[0]*the_range[0])/2.
    print("Calculation Result=% 5.2f; Real Result=% 5.2f; Error=% 5.2e" % \
         (result, real_result, result-real_result))

    print("\n\nFor integrand function f(x,y)=sin(x) ", \
              "the integral is-(cos(Range[1])-cos(Renge[0]))")
    result = integration(lambda x, y: math.sin(x), the_range)
    real_result = -(math.cos(the_range[1])-math.cos(the_range[0]))
    print("Calculation Result=% 5.2f; Real Result=% 5.2f; Error=% 5.2e" % \
         (result, real_result, result-real_result))

    print("\n\nFor differential equetion dy/dx=f(x,y)=y ", \
               "the solution is y0*exp(Range[1]-Renge[0])")
    y_0 = 1
    result = integration(lambda x, y: y, the_range, y_0)
    real_result = y_0*math.exp(the_range[1]-the_range[0])
    print("Calculation Result=% 5.2f; Real Result=% 5.2f; Error=% 5.2e" % \
         (result, real_result, result-real_result))

    print("\n\nFor differential equetion dy/dx=x/y ", \
               "the solution is sqrt(Range[1]^2-Renge[0]^2+y0^2)")
    y_0 = 1
    result = integration(lambda x, y: x/y, the_range, y_0)
    real_result = math.sqrt(the_range[1]*the_range[1]-the_range[0]*the_range[0]+y_0*y_0)
    print("Calculation Result=% 5.2f; Real Result=% 5.2f; Error=% 5.2e" % \
         (result, real_result, result-real_result))

runge_kutta_test_function()
print("\n\n")
