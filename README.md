# Runge-Kutta method


**1 Overview**

1.1 The project is intended for registration on the Python Development Course (SoftServe).

1.2 The project implements the numerical integration by the Runge-Kutta fourth order method.


**2 Overview of Runge-Kutta methods**

2.1 A first-order ordinary differential equation contains one independent variable (x), one function (y(x)) of this independent variable, and the first derivative of this function (dy/dx):

dy/dx =f(x,y),
where f(x,y) is a function of x and y. 

2.2 The Runge-Kutta methods are family of iterative methods for approximate solution of first-order ordinary differential equations. The Runge-Kutta methods use a discrete step of integration with a trial steps at the midpoint in order to decrease lower-order error term. 
Suppose that: there is defined the first-order ordinary differential equation dy/dx =f(x,y) with independent variable x, unknown function y(x), and specified function f(x,y); there is defined the initial value of function y0 at x=x0 (y0=y(x0)); the integration step h is specified. One-step procedure of Runge-Kutta method numerically calculates the approximate value of the function y at x=x0+h (y(x0+h)). 
Order of a Runge-Kutta method defines the number trial steps at the midpoint. Thereafter the trial steps are called iterations of Runge-Kutta method.

2.3 The forth order Runge-Kutta method is mostly commonly used. One-step procedure of this Runge-Kutta method consists of fourth sequential iterations. Each iteration calculates the respective iteration value (Kn where n – iteration number) base on an iteration value of a previous iteration:

K1=f(x0,y0);

K2=f(x0+h/2, y0+K1*h/2);

K3=f(x0+h/2, y0+K2*h/2);

K4=f(x0+h, y0+K3*h/2).

The result of one step procedure is the function value at x0+h:

y(x0+h)=y(x0)+K1*(h/6) +K2*(h*2/6) +K3*(h*2/6) +K4*(h/6).

2.4 In order to calculate a function value at x=x0+N*h, the one-step procedure of Runge-Kutta method is applied sequentially N times. Each next one-step procedure assigns x0->x0+h and y0->y(x0+h).

2.5 If the function f(x,y) depends only on x and does not depend on y (f(x)), then the first-order ordinary differential equation transforms to the integration:

$$y= \int_{x0}^{x0+Nh} f(x)dx$$.

In this case the Runge-Kutta method performs integration of an integrand function f(x). The initial value y0 by convention equals zero (y0=0).

2.6 The accuracy of the Runge-Kutta method depends on the method order and on the specified integration step h. 


**3 Programming implementation of the forth order Runge-Kutta method**

3.1 The project “RungeKutta” contains a Python realization of the described fourth order Runge-Kutta method, which implemented in a function integration()  (module: runge-kutta.py). 

3.2 The function integration() has four parameters:

1) integrand - a pointer to a function f(x,y) with two floating point parameters, the function returns a floating point value result of the function;

2) integ_range – a list of two values which specify a integration range (interval), integ_range[0] – beginning of the interval, integ_range[1] – end of the interval;

3) y_0 – an initial function value at the beginning of the interval (y_0=y(x0)= y(integ_range[0]));

4) step – Runge-Kutta integration step (above denoted as h).

The function integration() returns the solution of the first-order ordinary differential equation: y(integ_range[1]). If f(x,y) depends only on x, then the function integration() returns an integral of the integrand function f(x) over interval integ_range.

3.3 The function integration() operates with following variables.

3.3.1 An integration variable “x_var” represents the independent variable (x). For each Runge-Kutta step procedure the value of this variable corresponds to the value of x0. For first step this variable equals to the start of the integration interval, therefore the variable is initialized by integ_range[0].

3.3.2 The numpy array “iteration_value” represents set of calculation values, which is used in the one-step Runge-Kutta procedure. 
The current function value (above denoted y(x)) is represented by first element of the array: iteration_value[0]=y(x). The element is a result of the Runge-Kutta calculation. Before the one-step Runge-Kutta procedure this element equals to the initial function value: iteration_value[0]=y0=y(x0). After the one-step procedure this element equals to the result: iteration_value[0]=y(x0+h). This element is initialized by the function parameter y_0 at the beginning.
The iteration values (above denoted Kn) are represented by next elements of the array. In case of fourth order Runge-Kutta method there are four iteration values: K1=iteration_value[1], K2=iteration_value[2], K3=iteration_value[3], K4=iteration_value[4].
The size of the iteration_value array equals to the order of the Runge-Kutta method plus one.

3.3.3 A numpy array “coef” represents a set of multipliers for a respective iteration value, which is used for the function value calculation. The elements of this array are [h/6, h*2/6, h*2/6, h/6] where h is integration step. The size of the array equals to the Runge-Kutta method order (4). With this array the expression

y(x0+h)=y(x0)+K1*(h/6) +K2*(h*2/6) +K3*(h*2/6) +K4*(h/6)

can be rewritten:

iteration_value[0]+= iteration_value[1]* coef[0]+ iteration_value[2]* coef[1] +iteration_value[3]* coef[2]+ iteration_value[4]* coef[3]

or

iteration_value[0]+= numpy.dot( iteration_value[1:], coef).

3.3.4 A numpy array “steps” represents the values, which are used for the calculation of respective iteration value. The array has size of the Runge-Kutta method order (4). The elements of this array are [0, h/2, h/2, h]. With this array the update of an iteration value is:

K[n]=f(x0+ steps[n-1], y0+K[n-1]* steps[n-1])    

where K corresponds to the iteration_value array. In case of K[1] calculation: the step value steps[0] equals zero; and K[0] corresponds to y0.

The iteration value update can be expressed with the introduced variables:

iteration_value[n]= integrand(x_var+ steps[n-1], iteration_value[0]+iteration_value[n-1]* steps[n-1]) ,

for n>=1 and n<Order.

3.4 The one-step Runge-Kutta procedure is performed within a while-cycle in the function integration().

3.4.1 Sequential updates of Runge-Kutta iteration values from second element of iteration_value array to the last element are realized with for-cycle. Each iteration value is updated as shown in the clause 3.3.4.

3.4.2 When the iteration values are calculated the function value is calculated according to procedure shown in the clause 3.3.3. The current value function rewrites in iteration_value[0]. 

3.4.3 The integration variable x_val is assigned to the new current value of the independent variable: x_val= x_val+step.

3.5 The one-step Runge-Kutta procedure is repeated up until the integration variable would reach the end of the integration interval. When the while-cycle is finished, the solution of the first-order ordinary differential equation is presented by iteration_value[0]. The function integration() returns this value as the result.


**4 Testing of the program**

4.1 The testing procedure consists of testing of the described above function integration(). The testing is comprised of the comparison of an analytical solution of a differential equation with well known function f(x,y) with an approximate solution of this  differential equation, which is achieved by the Runge-Kutta method.
A function runge_kutta_test_function()  (module: runge-kutta.py) implements the testing procedure.

4.2 The following functions with know solutions are tested within the integration range [a,b]:

1) Integrals:
- f(x,y)=0, the solution is y=0;
- f(x,y)=1, the solution is y=b-a;
- f(x,y)=k, the solution is y=k*(b-a);
- f(x,y)=x, the solution is y= (b^2-a^2)/2;
- f(x,y)=sin(x), the solution is y=-(cos(b)-cos(a));
2) differential equations with y0:
- f(x,y)=y, the solution is y=y0*exp(x);
- f(x,y)=x/y, the solution is y=sqrt(b^2-a^2+y0^2).
4.3 An absolute error as a difference between the calculated approximate solution and respective known solution is chosen for testing objective.

